const withImages = require("next-images");
const withPlugins = require("next-compose-plugins");

const withImagesConfig = {
  webpack: (config, options) => config,
};

const plugins = [
  [withImages, withImagesConfig],

];

const nextConfig = {};
module.exports = withPlugins(plugins, nextConfig);
