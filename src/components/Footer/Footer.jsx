import { useAnimation } from 'framer-motion';
import React, { useEffect, useState } from "react";
import {
  AiFillGitlab,
  AiFillInstagram,
  AiFillLinkedin,
} from "react-icons/ai";
import { FaTelegramPlane } from "react-icons/fa";
import { useInView } from 'react-intersection-observer';


import { SocialIcons } from "../Header/HeaderStyles";
import {
  CompanyContainer,
  FooterWrapper,
  LinkColumn,
  LinkItem,
  LinkList,
  LinkTitle,
  Slogan,
  SocialContainer,
  SocialIconsContainer,
} from "./FooterStyles";

const Footer = () => {
    const [isAlreadyAnimated, setAlreadyAnimated] = useState(false);
    const { ref, inView } = useInView({ threshold: 0.5 });
    const animation = useAnimation();

    useEffect(() => {
      if (!isAlreadyAnimated) {
        if (inView) {
          animation.start({
            y: 0,
            opacity: 1,
            scale: 1,
            transition: { type: "spring", duration: 1.5, bounce: 0.1 },
          });
          setAlreadyAnimated(true);
        } else {
          animation.start({
            opacity: 0,
            y: 0,
            scale: 0.8,
          });
        }
      }
    }, [inView]);

  return (
    <FooterWrapper animate={animation} ref={ref}>
      <LinkList>
        <LinkColumn>
          <LinkTitle>Call</LinkTitle>
          <LinkItem href="tel:380960270113">+38-096-027-01-13</LinkItem>
        </LinkColumn>
        <LinkColumn>
          <LinkTitle>Email</LinkTitle>
          <LinkItem href="mailto:mmaximonchuk@gmail.com">
            mmaximonchuk@gmail.com
          </LinkItem>
        </LinkColumn>
      </LinkList>
      <SocialIconsContainer>
        <CompanyContainer>
          <Slogan>Innovating one project at a time</Slogan>
        </CompanyContainer>
        <SocialContainer isFooter="true">
          <SocialIcons target="_blank" href="https://tlgg.ru/@never_be_typical">
            <FaTelegramPlane size="3rem" />
          </SocialIcons>
          <SocialIcons target="_blank" href="https://gitlab.com/mmaximonchuk">
            <AiFillGitlab size="3rem" />
          </SocialIcons>
          <SocialIcons
            target="_blank"
            href="https://www.linkedin.com/in/maxim-simonchuk-8493b5190/"
          >
            <AiFillLinkedin size="3rem" />
          </SocialIcons>
          <SocialIcons
            target="_blank"
            href="https://www.instagram.com/devoted_person_/"
          >
            <AiFillInstagram size="3rem" />
          </SocialIcons>
        </SocialContainer>
      </SocialIconsContainer>
    </FooterWrapper>
  );
};

export default Footer;
