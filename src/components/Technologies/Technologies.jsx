import React, { useEffect, useState } from "react";
import TextLoop from "react-text-loop";
import { useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { DiFirebase, DiHtml5, DiReact } from "react-icons/di";
import { SectionDivider, SectionText } from "../../styles/GlobalComponents";
import {
  CustomSection,
  List,
  ListContainer,
  ListItem,
  ListParagraph,
  ListTitle,
  SectionTitleWithPadding,
} from "./TechnologiesStyles";

const Technologies = () => {
  const [isAlreadyAnimated, setAlreadyAnimated] = useState(false);
  const { ref, inView } = useInView({threshold: 0.5});
  const animation = useAnimation();
  const animationSkills = useAnimation();
  
  const variantsSkills = {
    animating: {
      transition: {
        staggerChildren: 1,
      },
    },
  };
  const variantsSkill = {
    hidden: {
      x: "100vw",
    },
    animating: {
      x: 0,
      transition: { ease: [0.6, 0.01, -0.05, 0.95], duration: 1 },
    },
  };

  useEffect(() => {
    if (!isAlreadyAnimated) {
      if (inView) {
        animation.start({
          y: 0,
          opacity: 1,
          scale: 1,
          transition: { type: "spring", duration: 1.5, bounce: 0.4 },
        });
        animationSkills.start("animating");
        setAlreadyAnimated(true);
      } else {
        animation.start({
          opacity: 0,
          y: 0,
          scale: 0.8,
        });
        animationSkills.start("hidden");
      }
    }
  }, [inView]);

  return (
    <CustomSection ref={ref} id="tech" animate={animation}>
      <SectionDivider />
      <SectionTitleWithPadding>Technologies</SectionTitleWithPadding>
      <SectionText>
        I've worked with a range of technologies in the web development world.
        From Front-end To Back-end
      </SectionText>

      <List animate={animationSkills} variants={variantsSkills}>
        <ListItem variants={variantsSkill}>
          <DiReact size="3rem" />
          <ListContainer>
            <ListTitle>Front-End</ListTitle>
            <ListParagraph>
              Experience with <br />
              <TextLoop adjustingSpeed={500}>
                <span>React.js</span>
                <span>Redux</span>
                <span>Next.js</span>
                <span>Typescript</span>
                <span>Gatsby.js</span>
                <span>Styled-components</span>
                <span>Ant Design</span>
              </TextLoop>
            </ListParagraph>
          </ListContainer>
        </ListItem>
        <ListItem variants={variantsSkill}>
          <DiHtml5 size="3rem" />
          <ListContainer>
            <ListTitle>HTML/CSS</ListTitle>
            <ListParagraph>
              Experience with <br />
              <TextLoop delay={1500} adjustingSpeed={500}>
                <span>Webpack</span>
                <span>SCSS</span>
                <span>SASS</span>
              </TextLoop>
            </ListParagraph>
          </ListContainer>
        </ListItem>
        <ListItem variants={variantsSkill}>
          <DiFirebase size="3rem" />
          <ListContainer>
            <ListTitle>Back-End</ListTitle>
            <ListParagraph>
              Experience with <br />
              Node.js and{" "}
              <TextLoop delay={3000} adjustingSpeed={500}>
                <span>SQL databases</span>
                <span>PostgreSQL</span>
                <span>MySQL</span>
                <span>Sequelize</span>
                <span>Firebase</span>
              </TextLoop>
            </ListParagraph>
          </ListContainer>
        </ListItem>
      </List>
    </CustomSection>
  );
};

export default Technologies;
