import React, { useEffect, useState } from "react";
import Image from "next/image";
import {
  Section,
  SectionDivider,
  SectionTitle,
} from "../../styles/GlobalComponents";

import { AcomplishmentsData } from "../../constants/constants";

import ImgAvatar from "./avatar.jpg";

import {
  AboutMeImageWrapper,
  AboutMeWerapper,
  Box,
  Boxes,
  BoxNum,
  BoxText,
  PersonalInformation,
} from "./AcomplishmentsStyles";
import { useInView } from 'react-intersection-observer';
import { useAnimation } from 'framer-motion';

const Acomplishments = () => {
    const [isAlreadyAnimated, setAlreadyAnimated] = useState(false);
    const { ref, inView } = useInView({ threshold: 0.7 });
    const animationSkills = useAnimation();
    const variantsSkill = {
      hidden: {
        y: "-100%",
      },
      animating: {
        y: 0,
        transition: {
          staggerChildren: 1,
          ease: [0.6, 0.01, -0.05, 0.95],
          duration: 1,
        },
      },
    };

  useEffect(() => {
    if (!isAlreadyAnimated) {
      if (inView) {
        animationSkills.start("animating");
        setAlreadyAnimated(true);
      } else {
        animationSkills.start("hidden");
      }
    }
  }, [inView]);

  return (
    <Section ref={ref}>
      <SectionTitle>Personal Acomplishments</SectionTitle>
      <Boxes animate={animationSkills} variants={variantsSkill}>
        {AcomplishmentsData.map((card, index) => {
          return (
            <Box
              variants={{
                hidden: {
                  y: index % 2 === 0 ? "-100%" : "100%",
                },
                animating: {
                  y: 0,
                  transition: {
                    staggerChildren: 1,
                    ease: [0.6, 0.01, -0.05, 0.95],
                    duration: 1,
                  },
                },
              }}
              key={index}
            >
              <BoxNum>{card.number}+</BoxNum>
              <BoxText>{card.text}</BoxText>
            </Box>
          );
        })}
      </Boxes>
    </Section>
  );
};

export default Acomplishments;
