import styled from "styled-components";
import {motion} from 'framer-motion'
export const Boxes = styled(motion.div)`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: 24px;
  margin: 24px 0 40px;
  overflow: hidden;
  @media ${(props) => props.theme.breakpoints.md} {
    gap: 16px;
    margin: 20px 0 32px;
    grid-template-columns: repeat(auto-fit, minmax(140px, 1fr));
  }

  @media ${(props) => props.theme.breakpoints.sm} {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    gap: 10px;
    max-width: 500px;
    margin: 24px auto;
  }
`;

export const Box = styled(motion.div)`
  background: ${(props) => props.theme.colors.rockPurple};
  border-radius: 12px;
  height: 144px;
  padding: 24px;
  @media ${(props) => props.theme.breakpoints.lg} {
    height: 210px;
  }

  @media ${(props) => props.theme.breakpoints.md} {
    height: 135px;
    padding: 16px;
  }

  @media ${(props) => props.theme.breakpoints.sm} {
    height: 110px;
    padding: 12px;

    &:nth-child(2n) {
      grid-row: 2;
    }
  }
`;
export const BoxNum = styled.h5`
  font-style: normal;
  font-weight: 600;
  font-size: 36px;
  line-height: 40px;
  letter-spacing: 0.01em;
  color: #ffffff;
  margin-bottom: 8px;

  @media ${(props) => props.theme.breakpoints.md} {
    font-size: 28px;
    line-height: 32px;
  }
  @media ${(props) => props.theme.breakpoints.sm} {
    font-size: 24px;
    line-height: 26px;
  }
`;

export const BoxText = styled.p`
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 24px;
  letter-spacing: 0.02em;
  color: rgba(255, 255, 255, 0.75);

  @media ${(props) => props.theme.breakpoints.md} {
    font-size: 16px;
    line-height: 20px;
  }

  @media ${(props) => props.theme.breakpoints.sm} {
    font-size: 10px;
    line-height: 14px;
  }
`;

export const Join = styled.div`
  display: flex;
  max-width: 1040px;
  justify-content: center;
  align-items: center;
  padding-bottom: 80px;

  @media ${(props) => props.theme.breakpoints.md} {
    display: flex;
    justify-content: center;
    padding-bottom: 64px;
  }

  @media ${(props) => props.theme.breakpoints.sm} {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-bottom: 32px;
  }
`;

export const JoinText = styled.h5`
  display: flex;
  font-size: 24px;
  line-height: 40px;
  letter-spacing: 0.02em;
  color: rgba(255, 255, 255, 0.5);

  @media ${(props) => props.theme.breakpoints.md} {
    line-height: 32px;
    font-size: 20px;
  }

  @media ${(props) => props.theme.breakpoints.sm} {
    font-size: 16px;
    line-height: 24px;
    margin: 0 0 16px;
  }
`;

export const IconContainer = styled.div`
  display: flex;

  @media ${(props) => props.theme.breakpoints.sm} {
    width: 160px;
    justify-content: space-between;
  }
`;

// Rainbow-fading text-shadow animation

export const AboutMeImageWrapper = styled.div`
  flex: 1;
  position: relative;
  width: 100%;
  animation: 3s linear infinite boxShadow;
  margin-bottom: 30px;

  @keyframes boxShadow {
    from {
      box-shadow: 5px 5px 6px 5px #ff8080, -4px 5px 6px 5px #ffe488,
        -4px -5px 6px 5px #8cff85, 6px -5px 6px 5px #80c7ff,
        6px 6px 6px 7px #e488ff, -4px 6px 6px 7px #ff616b,
        -4px -4px 14px 1px #8e5cff, 5px 5px 6px 5px rgba(0, 0, 0, 0);
    }
    50% {
      box-shadow: 3px 3px 6px 5px #8e5cff, -4px 3px 6px 5px #80c7ff,
        -5px -3px 6px 5px #ff616b, 6px -3px 6px 5px #ffe488,
        6px 6px 6px 7px #e488ff, -6px 6px 6px 7px #8cff85,
        -6px -5px 14px 1px #ff616b, 3px 3px 6px 5px rgba(0, 0, 0, 0);
    }
    to {
      box-shadow: 5px 5px 6px 5px #ff8080, -4px 5px 6px 5px #ffe488,
        -4px -5px 6px 5px #8cff85, 6px -5px 6px 5px #80c7ff,
        6px 6px 6px 7px #e488ff, -4px 6px 6px 7px #ff616b,
        -4px -4px 14px 1px #8e5cff, 5px 5px 6px 5px rgba(0, 0, 0, 0);
    }
  }
`;

export const AboutMeWerapper = styled.div`
  gap: 30px;
  display: flex;
`;
export const PersonalInformation = styled.div`
  flex: 1;
  .title {
    font-size: 40px;
  }
  .description {
    color: hsla(0, 0%, 100%, 0.8);
    font-size: 15px;
  }
`;
