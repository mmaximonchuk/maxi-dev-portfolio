import React, { useState, useEffect } from "react";
import { motion } from "framer-motion";

import { SectionText, SectionTitle } from "../../styles/GlobalComponents";
import Button from "../../styles/GlobalComponents/Button";
import { LeftSection, StyledHeroSection } from "./HeroStyles";


const Hero = ({ startAnimation }) => {
  const containerVariants = {
    animating: {
      transition: {
        staggerChildren: 0.5,
      },
    },
  };
  const itemVariants = {
    initial: {
      x: -200,
      opacity: 0,
    },
    animating: {
      x: 0,
      opacity: 1,
      transition: { ease: [0.6, 0.01, -0.05, 0.95], duration: 2 },
    },
  };

  return (
    <StyledHeroSection row>
      <LeftSection>
        {/* <FileViewer
          fileType="pdf"
          filePath="../../../static/Maxim_Simonchuk_CV.pdf"
        /> */}
        <motion.div
          variants={containerVariants}
          initial="initial"
          animate="animating"
        >
          <motion.div variants={itemVariants}>
            <SectionTitle className="" main center>
              <span className="sign flicker">Welcome</span> to <br />
              Maxi-dev Portfolio
            </SectionTitle>
          </motion.div>
          <motion.div variants={itemVariants}>
            <SectionText>
              The puprope of My Portfolio is to let recruiters and clients know
              what kind of skills and technologies i've aquired and still
              continue to master.
            </SectionText>
          </motion.div>
          <motion.div variants={itemVariants}>
            <SectionText>
              I'm a Freelancer Front-end Developer with over 1 years of
              experience. I'm from Kyiv. I code and create web elements for
              amazing people around the world. I like work with new people. New
              people new Experiences.
            </SectionText>
          </motion.div>
          <motion.div variants={itemVariants}>
            <Button
              href={`https://maxi-dev-portfolio.vercel.app/Maxim_Simonchuk_CV.pdf`}
              download
            >
              Download CV
            </Button>
          </motion.div>
        </motion.div>
      </LeftSection>
    </StyledHeroSection>
  );
};

export default Hero;
