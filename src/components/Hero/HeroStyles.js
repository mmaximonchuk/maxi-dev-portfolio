import { motion } from 'framer-motion';
import styled from "styled-components";
import { Section } from "../../styles/GlobalComponents";

export const StyledHeroSection = styled(Section)`
  padding: 0;
  margin-top: 30px;
  
  @media ${(props) => props.theme.breakpoints.sm} {
    margin-top: 0;
  }
`;

export const LeftSection = styled(motion.div)`
  width: 100%;
  @media ${(props) => props.theme.breakpoints.sm} {
    width: 80%;
    display: flex;
    flex-direction: column;

    margin: 0 auto;
  }
  @media ${(props) => props.theme.breakpoints.md} {
    width: 100%;
    display: flex;
    flex-direction: column;

    margin: 0 auto;
  }
`;
