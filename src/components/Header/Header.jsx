import React, { useEffect, useState } from "react";
import Link from "next/link";
import { motion } from "framer-motion";

import { AiFillGitlab, AiFillInstagram, AiFillLinkedin } from "react-icons/ai";
import { FaTelegramPlane } from "react-icons/fa";
import { DiCssdeck } from "react-icons/di";
import BurgerIcon from "./BurgerIcon";

import {
  Container,
  ContainerInner,
  Div1,
  Div2,
  Div3,
  NavLink,
  SocialIcons,
  BurgerButtonMenu,
} from "./HeaderStyles";

const Header = () => {
  const [isOpenedMenu, setOpenedMenu] = useState(false);
  const [scrollValue, setScrollVlaue] = useState(0);

  useEffect(() => {
    window?.addEventListener("scroll", handleScrollFunction);
    return () => window?.removeEventListener("scroll", handleScrollFunction);
  }, []);

  const toggleOpenMenu = (event) => {
    setOpenedMenu(!isOpenedMenu);
  };

  const handleScrollFunction = () => setScrollVlaue(window?.scrollY);

  // use it in later sections
  // const headerBanner = {
  //   animating: {
  //     transition: {
  //       delayChildren: 0.4,
  //       staggerChildren: 0.1,
  //     },
  //   },
  // };
  // const laterAnimation = {
  //   initial: {
  //     y: 400,
  //   },
  //   animating: {
  //     y: 0,
  //     transition: { ease: [0.6, 0.01, -0.05, 0.95], duration: 1 },
  //   },
  // };
  return (
    <Container scrollValue={scrollValue}>
      <ContainerInner
        initial={{ opacity: 0, y: -180 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ ease: "easeInOut", duration: 1, delay: 2 }}
        scrollValue={scrollValue}
      >
        <Div1>
          <Link href="#">
            <motion.a
              style={{ display: "flex", alignItems: "center", color: "white" }}
            >
              <DiCssdeck size="3rem" />{" "}
              <span className="logo-text">Portfolio</span>
            </motion.a>
          </Link>
        </Div1>

        <HeaderDropdown isOpenedMenu={isOpenedMenu} />
        <BurgerButtonMenu ariaExpanded={isOpenedMenu} onClick={toggleOpenMenu}>
          <button className={`menu ${isOpenedMenu ? "opened" : ""}`}>
            <BurgerIcon />
          </button>
        </BurgerButtonMenu>
      </ContainerInner>
    </Container>
  );
};

export default Header;

const HeaderDropdown = ({ isOpenedMenu }) => {
  return (
    <>
      <Div2 isOpenedMenu={isOpenedMenu}>
        <li>
          <Link href="#projects">
            <NavLink>Projects</NavLink>
          </Link>
        </li>
        <li>
          <Link href="#tech">
            <NavLink>Technologies</NavLink>
          </Link>
        </li>
        <li>
          <Link href="#about">
            <NavLink>About</NavLink>
          </Link>
        </li>
      </Div2>
      <Div3 isOpenedMenu={isOpenedMenu}>
        <SocialIcons target="_blank" href="https://tlgg.ru/@never_be_typical">
          <FaTelegramPlane size="3rem" />
        </SocialIcons>
        <SocialIcons target="_blank" href="https://gitlab.com/mmaximonchuk">
          <AiFillGitlab size="3rem" />
        </SocialIcons>
        <SocialIcons
          target="_blank"
          href="https://www.linkedin.com/in/maxim-simonchuk-8493b5190/"
        >
          <AiFillLinkedin size="3rem" />
        </SocialIcons>
        <SocialIcons
          target="_blank"
          href="https://www.instagram.com/devoted_person_/"
        >
          <AiFillInstagram size="3rem" />
        </SocialIcons>
      </Div3>
    </>
  );
};
