import { motion } from 'framer-motion';
import { IoIosArrowDropdown } from "react-icons/io";
import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  transition: background 0.3s, box-shadow 0.3s;
  position: ${({ scrollValue }) => (scrollValue > 5 ? "fixed" : "fixed")};
  background: ${({ scrollValue }) =>
    scrollValue > 5 ? "#33006F" : "transparent"};

  width: ${({ scrollValue }) => (scrollValue > 5 ? "100vw" : "100%")};
  box-shadow: ${({ scrollValue }) =>
    scrollValue > 5 ? "0 -7px 23px #fff" : "none"};
  z-index: 3;
  justify-content: center;
  left: 0;
  top: 0;
  @media ${(props) => props.theme.breakpoints.sm} {
  }
`;

export const ContainerInner = styled(motion.div)`
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  grid-template-rows: 1fr;
  max-width: 1280px;
  padding: ${({ scrollValue }) => (scrollValue > 5 ? "1rem" : "2.5rem 1rem")};
  transition: padding 0.3s;
  z-index: 2;
  width: 100%;
  @media ${(props) => props.theme.breakpoints.sm} {
    grid-template-columns: repeat(5, 1fr);
    grid-template-rows: repeat(1, 60px);
    grid-column-gap: 0.5rem;
    grid-row-gap: 0.5rem;
  }
`;

export const BurgerButtonMenu = styled(motion.div).attrs(({ ariaExpanded }) => ({
  "aria-expanded": ariaExpanded,
  "aria-label": "Main Menu",
}))`
  grid-area: 1 / 6;
  display: none;
  z-index: 1;
  .menu {
    background-color: transparent;
    border: none;
    cursor: pointer;
    display: flex;
    padding: 0;
  }
  .line {
    fill: none;
    stroke: rgba(255, 255, 255, 0.75);
    stroke-width: 6;
    transition: stroke-dasharray 600ms cubic-bezier(0.4, 0, 0.2, 1),
      stroke-dashoffset 600ms cubic-bezier(0.4, 0, 0.2, 1);
  }
  .line1 {
    stroke-dasharray: 60 207;
    stroke-width: 6;
  }
  .line2 {
    stroke-dasharray: 60 60;
    stroke-width: 6;
  }
  .line3 {
    stroke-dasharray: 60 207;
    stroke-width: 6;
  }
  .opened {
    .line1 {
      stroke-dasharray: 90 207;
      stroke-dashoffset: -134;
      stroke-width: 6;
    }
    .line2 {
      stroke-dasharray: 1 60;
      stroke-dashoffset: -30;
      stroke-width: 6;
    }
    .line3 {
      stroke-dasharray: 90 207;
      stroke-dashoffset: -134;
      stroke-width: 6;
    }
  }
  @media ${(props) => props.theme.breakpoints.sm} {
    display: grid;
  }
`;
export const Div1 = styled.div`
  grid-area: 1 / 1 / 2 / 2;
  margin-right: 20px;
  display: flex;
  flex-direction: row;
  align-content: center;
  .logo-text {
    font-size: 23px;
  }
  @media ${(props) => props.theme.breakpoints.sm} {
    grid-area: 1 / 1 / 2 / 3;
    z-index: 2;
  }
`;
export const Div2 = styled.div`
  grid-area: 1 / 3;
  display: flex;
  justify-content: space-around;
  pointer-events: none;
  align-items: center;
  z-index: 5;
  li {
    pointer-events: auto;
    margin-right: 12px;
    z-index: 5;
  }
  @media ${(props) => props.theme.breakpoints.md} {
    grid-area: 1 / 2;
  }
  @media ${(props) => props.theme.breakpoints.sm} {
    display: ${({ isOpenedMenu }) => (isOpenedMenu ? "flex" : "flex")};
    flex-direction: column;
    opacity: ${({ isOpenedMenu }) => (isOpenedMenu ? 1 : 0)};
    transition-property: opacity;
    transition-duration: 0.3s;
    background-color: ${({ isOpenedMenu }) =>
      isOpenedMenu ? "rgba(0, 0, 0, 0.9)" : "transparent"};
    width: 100vw;
    height: 100vh;
    position: fixed;
    left: 0;
    top: 0;
    bottom: 0;
    z-index: 1;
    justify-content: flex-start;
    padding-top: 110px;
    gap: 45px;
  }
`;
export const Div3 = styled.div`
  grid-area: 1 / 5 / 2 / 5;
  display: flex;
  justify-content: space-around;
  align-items: center;
  transform-origin: 50px 60px;
  transition-property: opacity transform-origin;
  transition-duration: ${({ isOpenedMenu }) =>
    isOpenedMenu ? "0.3s" : "none"};
  transition-delay: ${({ isOpenedMenu }) => (isOpenedMenu ? "0.5s" : "none")};
  a {
    display: flex;
    align-items: center;
  }
  @media ${(props) => props.theme.breakpoints.sm} {
    display: ${({ isOpenedMenu }) => (isOpenedMenu ? "flex" : "flex")};
    opacity: ${({ isOpenedMenu }) => (isOpenedMenu ? 1 : 0)};
    align-items: center;
    bottom: 100px;
    left: 50%;
    transform: translateX(-50%);
    position: ${({ isOpenedMenu }) => (isOpenedMenu ? "fixed" : "static")};
    z-index: 1;
  }
`;

// Navigation Links
export const NavLink = styled.a`
  font-size: 2rem;
  line-height: 32px;
  color: rgba(255, 255, 255, 0.75);
  transition: 0.4s ease;
  &:hover {
    color: #fff;
    opacity: 1;
    cursor: pointer;
  }
  @media ${(props) => props.theme.breakpoints.sm} {
    padding: 0.5rem;
    font-size: 4rem;
  }
  @media ${(props) => props.theme.breakpoints.xs} {
    font-size: 3rem;
  }
`;

/// DropDown Contact
export const ContactDropDown = styled.button`
  border: none;
  display: flex;
  position: relative;
  background: none;
  font-size: 1.7rem;

  line-height: 32px;
  color: rgba(255, 255, 255, 0.75);
  cursor: pointer;
  transition: 0.3s ease;

  &:focus {
    outline: none;
  }
  &:hover {
    color: #fff;
  }

  @media ${(props) => props.theme.breakpoints.sm} {
    padding: 0.4rem 0;
  }
  @media ${(props) => props.theme.breakpoints.md} {
    padding: 0;
  }
`;

export const NavProductsIcon = styled(IoIosArrowDropdown)`
  margin-left: 8px;
  display: flex;
  align-self: center;
  transition: 0.3s ease;
  opacity: ${({ isOpen }) => (isOpen ? "1" : ".75")};
  transform: ${({ isOpen }) => (isOpen ? "scaleY(-1)" : "scaleY(1)")};

  &:hover {
    opacity: 1;
  }

  @media ${(props) => props.theme.breakpoints.sm} {
    margin: 2px 0 0 2px;
    width: 15px;
  }
`;

// Social Icons

export const SocialIcons = styled.a`
  transition: 0.3s ease;
  color: white;
  border-radius: 50px;
  padding: 8px;
  display: flex;
  align-items: center;
  &:hover {
    background-color: #4c2a78;
    transform: scale(1.2);
    cursor: pointer;
  }
  @media ${(props) => props.theme.breakpoints.sm} {
    & svg {
      width: 5rem;
      height: 5rem;
    }
  }
  @media ${(props) => props.theme.breakpoints.xs} {
    & svg {
      width: 3rem;
      height: 3rem;
    }
  }
`;
