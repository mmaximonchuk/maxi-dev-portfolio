import React, { createRef, useEffect, useState } from "react";
import { useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
import {
  BlogCard,
  CardInfo,
  ExternalLinks,
  GridContainer,
  HeaderThree,
  Hr,
  Tag,
  TagList,
  TitleContent,
  UtilityList,
  Img,
  ImgWrapper,
} from "./ProjectsStyles";

import {
  Section,
  SectionDivider,
  SectionTitle,
} from "../../styles/GlobalComponents";

import { projects } from "../../constants/constants";

const Projects = () => {
  const [isAlreadyAnimated, setAlreadyAnimated] = useState(false);
  const { ref, inView } = useInView({ threshold: 0.2 });
  const animation = useAnimation();

  useEffect(() => {
    if (!isAlreadyAnimated) {
      if (inView) {
        animation.start({
          y: 0,
          opacity: 1,
          scale: 1,
          transition: { type: "spring", duration: 1.5, bounce: 0.4 },
        });
        setAlreadyAnimated(true);
      } else {
        animation.start({
          opacity: 0,
          y: 0,
          scale: 0.8,
        });
      }
    }
  }, [inView]);

  return (
    <Section
      id="projects"
      animate={animation}
      ref={ref}
    >
      <SectionDivider />
      <SectionTitle main>
        Projects
      </SectionTitle>
      <GridContainer>
        {projects.map(
          ({ id, image, title, description, tags, source, visit }) => {
            return (
              <BlogCard key={id}>
                <ImgWrapper>
                  <Img width="900px" height="455px" src={image} />
                </ImgWrapper>
                <TitleContent>
                  <HeaderThree title="true">{title}</HeaderThree>
                  <Hr />
                </TitleContent>
                <CardInfo>{description}</CardInfo>
                <div>
                  <TitleContent>Stack</TitleContent>
                  <TagList>
                    {tags.map((tag) => (
                      <Tag key={tag}>{tag}</Tag>
                    ))}
                  </TagList>
                </div>
                <UtilityList>
                  <ExternalLinks href={visit}>Code</ExternalLinks>
                  <ExternalLinks href={source}>Source</ExternalLinks>
                </UtilityList>
              </BlogCard>
            );
          }
        )}
      </GridContainer>
    </Section>
  );
};

export default Projects;


