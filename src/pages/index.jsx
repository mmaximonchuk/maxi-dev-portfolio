import { AnimateSharedLayout } from "framer-motion";
import Head from "next/head";
import { useEffect, useState } from "react";
import Acomplishments from "../components/Acomplishments/Acomplishments";
import BgAnimation from "../components/BackgrooundAnimation/BackgroundAnimation";
import Hero from "../components/Hero/Hero";
import Projects from "../components/Projects/Projects";
import Technologies from "../components/Technologies/Technologies";
import Timeline from "../components/TimeLine/TimeLine";
import { useResizeScreen } from "../hooks/useResizeScreen";
import { Layout } from "../layout/Layout";
import {
  BgAnimationWrapper,
  SpecialFirstSection,
} from "../styles/GlobalComponents";

const screenHeight =
  typeof window !== "undefined" ? window.innerHeight : "calc(100vh + 10px)";

const Home = () => {
  // const [startAnimation, setStartAnimation] = useState(false);
  const { isMobile } = useResizeScreen();

  return (
    <AnimateSharedLayout>
      <Layout>
        <Head>
          <title>Maxi-dev Portfolio</title>
        </Head>
        <SpecialFirstSection screenHeight={screenHeight} grid>
          <Hero />
          <BgAnimationWrapper>
            <BgAnimation isMobile={isMobile} />
          </BgAnimationWrapper>
        </SpecialFirstSection>
        <Projects />
        <Technologies />
        <Timeline />
        <Acomplishments />
      </Layout>
    </AnimateSharedLayout>
  );
};

export default Home;
