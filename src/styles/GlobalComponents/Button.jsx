import React from "react";

import { ButtonBack, ButtonFront } from "./index";

const Button = ({ alt, form, disabled, onClick, children, ...restProps }) => (
  <ButtonBack
    alt={alt}
    form={form}
    disabled={disabled}
    whileHover={{ scale: 1.05 }}
    whileTap={{ scale: 0.95 }}
    {...restProps}
  >
    {children}
    <ButtonFront alt={alt} onClick={onClick} disabled={disabled}>
      {children}
    </ButtonFront>
  </ButtonBack>
);

export default Button;
