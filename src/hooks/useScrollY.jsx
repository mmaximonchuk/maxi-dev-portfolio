import { useEffect, useState } from "react";

export const useScrollY = () => {
  const [scrollY, setScrollY] = useState(0);

  const culcScrollY = () => {
    setScrollY(window.scrollY);
  };

  useEffect(() => {
    window.addEventListener("scroll", culcScrollY);
    return () => window.removeEventListener("scroll", culcScrollY);
  }, []);

  return scrollY;
};
