import { useEffect, useState } from "react";

export const useResizeScreen = () => {
  const [screenWidth, setScreenWidth] = useState(() => {
    if (typeof window !== "undefined") {
      return window.innerWidth
    }
    return 0;
  });
  let isMobile = screenWidth < 641 ? true : false;
  let isDesktop = screenWidth > 641 ? true : false;

  const handleResize = () => {
    setScreenWidth(window.innerWidth);
  };

  useEffect(() => {
    setScreenWidth(window.innerWidth);
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return { isMobile, isDesktop };
};
